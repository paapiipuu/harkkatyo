package com.example.harkkatyo;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class AccountsFragment extends Fragment implements recyclerViewClass.OnAccountListener {

    private View view;
    private TextView errorMessage;
    private UserClass user;
    private AccountClass account = null;
    private DbManager db;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_accounts, container, false);
        errorMessage = view.findViewById(R.id.popupAccounts);

        user = UserClass.getInstance();

        RecyclerView recycler = view.findViewById(R.id.recyclerView);
        recyclerViewClass adapter = new recyclerViewClass(getContext(), user.getAccounts(), this);
        recycler.setAdapter(adapter);
        recycler.setLayoutManager(new LinearLayoutManager(getContext()));
        RecyclerView.ItemDecoration divider = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        recycler.addItemDecoration(divider);

        return view;
    }

    //when account is clicked from the recycler view method saves it to attribute
    //method is from recyclerViewClass ->override
    @Override
    public void onAccountClick(int position) {
        errorMessage.setVisibility(View.INVISIBLE);
        user = UserClass.getInstance();
        ArrayList<AccountClass> accounts = user.getAccounts();
        account = accounts.get(position);
    }

    public AccountClass getChosenAccount() {
        if(account == null) {
            //no account was chosen
            errorMessage.setVisibility(View.VISIBLE);
        }
        AccountClass accountToActivity = account;
        return accountToActivity;
    }

    public void makeAccountNull() {
        account = null;
    }

    //is called from BaseActivity to call DbManager method to delete chosen account from database
    //if delete is successful returns true
    public boolean deleteChosenAccount() {
        if(account == null) {
            //no account was chosen
            errorMessage.setVisibility(View.VISIBLE);
            return false;
        }else{
            db = DbManager.getInstance();
            db.deleteAccount(account.getAccountNumber());
            account = null;
            return true;
        }
    }
}
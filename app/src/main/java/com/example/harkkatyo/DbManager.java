package com.example.harkkatyo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

class DbManager {
    private static final DbManager ourInstance = new DbManager();
    private SQLiteDatabase db = null;
    private DbHelper helper;

    static DbManager getInstance() {
        return ourInstance;
    }

    private DbManager() {}

    //Gets access to database through DbHelper
    //Method is called in MainActivities OnCreate
    public void open(Context context) {
        helper = new DbHelper(context);
        db = helper.getWritableDatabase();
        db.setForeignKeyConstraintsEnabled(true);
    }

    //Is called to return bank's information for BankClass when User or Admin logs in
    //Database only has one bank (only one was required) so query can be done without selection values
    public ArrayList getBank() {
        ArrayList<String> bank = new ArrayList<String>();

        String[] take = {"bankid", "bankname"};
        Cursor cursor = db.query("bank", take, null, null, null, null, null);

        while(cursor.moveToNext()) {
            String bankid = cursor.getString(cursor.getColumnIndex("bankid"));
            String bankname = cursor.getString(cursor.getColumnIndex("bankname"));
            bank.add(bankid);
            bank.add(bankname);
        }
        cursor.close();

        return bank;
    }

    //Checks that user's email isn't already in the database so it new user can be created with
    //email being the primary key
    public Boolean emailIsUnique(String compareEmail) {

        Boolean isUnique = true;

        //Comparing strings to users' login info
        String[] take = {"email"};

        Cursor cursor = db.query("user", take, null, null, null, null, null);
        while(cursor.moveToNext()) {
            if(compareEmail.equals(cursor.getString(0))) {
                isUnique = false;
            }
        }
        cursor.close();

        return isUnique;
    }

    //Inserts new user and town to database with given attributes
    public void createUser(String givenEmail, String givenFirstname, String givenLastname, Integer givenHouseNo, String givenStreet, Integer givenPostalcode, String givenCity, String givenPassword) {

        ContentValues userValues = new ContentValues();
        ContentValues townValues = new ContentValues();

        BankClass bank = BankClass.getInstance();
        userValues.put("bankid", bank.getBankId());

        townValues.put("bankid", bank.getBankId());
        townValues.put("postalcode", givenPostalcode);
        townValues.put("city", givenCity);
        townValues.put("useremail", givenEmail);
        db.insert("town", null, townValues);

        userValues.put("email", givenEmail);
        userValues.put("firstname", givenFirstname);
        userValues.put("lastname", givenLastname);
        userValues.put("password", givenPassword);
        userValues.put("housenumber", givenHouseNo);
        userValues.put("street", givenStreet);
        userValues.put("postcode", givenPostalcode);
        db.insert("user", null, userValues);
    }

    //Returns true if given login information matches admin's in database (-> user is admin)
    public boolean loginInfoCorrectAdmin(String loginEmail, String loginPassword) {

        boolean isEqual = false;
        String[] take = {"email" , "password"};
        Cursor cursor = db.query("admin", take, null, null, null, null, null);
        cursor.moveToNext();

        if(loginEmail.equals(cursor.getString(0)) && loginPassword.equals(cursor.getString(1))) {
            isEqual = true;
        }
        cursor.close();

        return isEqual;
    }

    //Returns true if given login information matches some user's in database (-> user is normal user)
    public boolean loginInfoCorrectUser(String loginEmail, String loginPassword) {

        boolean isSame = false;
        String[] take2 = {"email" , "password"};

        Cursor cursor2 = db.query("user", take2, null, null, null, null, null);
        while(cursor2.moveToNext()) {
            if(loginEmail.equals(cursor2.getString(0)) && loginPassword.equals(cursor2.getString(1))) {
                isSame = true;
            }
        }
        cursor2.close();

        return isSame;
    }

    //Gets user's informations with rawQuery, joining user and town tables, returns them as ArrayList
    public ArrayList getUserInfos(String wantedEmail) {
        ArrayList<String> userArraylist = new ArrayList<String>();

        String[] selectionArgs = { wantedEmail };

        final String MY_QUERY = "SELECT user.bankid, firstname, lastname, password, housenumber, street, postcode, city FROM user AS user INNER JOIN town AS town ON user.email=town.useremail WHERE user.email=?";

        Cursor cursor = db.rawQuery(MY_QUERY, selectionArgs);

        cursor.moveToNext();
        userArraylist.add(cursor.getString(cursor.getColumnIndex("bankid")));
        userArraylist.add(cursor.getString(cursor.getColumnIndex("firstname")));
        userArraylist.add(cursor.getString(cursor.getColumnIndex("lastname")));
        userArraylist.add(cursor.getString(cursor.getColumnIndex("password")));
        userArraylist.add(cursor.getString(cursor.getColumnIndex("housenumber")));
        userArraylist.add(cursor.getString(cursor.getColumnIndex("street")));
        userArraylist.add(cursor.getString(cursor.getColumnIndex("postcode")));
        userArraylist.add(cursor.getString(cursor.getColumnIndex("city")));
        cursor.close();

        return userArraylist;
    }

    //Gets every user email from database for Admin's view
    public ArrayList<String> getUserEmails() {
        ArrayList<String> usersEmailArraylist = new ArrayList<String>();

        String[] take = {"email"};
        Cursor cursor = db.query("user", take, null, null, null, null, null);

        while(cursor.moveToNext()) {
            String useremail = cursor.getString(cursor.getColumnIndex("email"));
            usersEmailArraylist.add(useremail);
        }
        cursor.close();

        return usersEmailArraylist;
    }

    //Deletes user with given email from database (only for admin)
    public void adminDeleteUser(String email) {
        String selection = "email = ?";
        String[] selectionArgs = { email };
        db.delete("user", selection, selectionArgs);
        String selection2 = "useremail = ?";
        db.delete("town", selection2, selectionArgs);
    }

    //Gets all accounts of user that has given email, returns them as ArrayList
    public ArrayList getAccounts(String email) {

        ArrayList<AccountClass> accountsArraylist = new ArrayList<AccountClass>();
        String selection = "useremail = ?";
        String[] selectionArgs = { email };
        String[] take = {"accountnumber", "balance", "useremail", "payingability"};
        Cursor cursor = db.query("account", take, selection, selectionArgs, null, null, null);

        while(cursor.moveToNext()) {
            Integer anumber = cursor.getInt(cursor.getColumnIndex("accountnumber"));
            Float abalance = cursor.getFloat(cursor.getColumnIndex("balance"));
            String auseremail = cursor.getString(cursor.getColumnIndex("useremail"));
            Boolean apay = Boolean.parseBoolean(cursor.getString(cursor.getColumnIndex("payingability")));
            AccountClass account = new AccountClass(anumber, abalance, auseremail, apay);
            accountsArraylist.add(account);
        }
        cursor.close();
        return accountsArraylist;
    }

    //Adds new account to the database and makes new accounts ArrayList
    //UserClasses accounts ArrayList is updated with new accounts so it includes the new one
    public void createNewAccount(Float balance, Boolean payability) {
        ArrayList<AccountClass> accountsArraylist;
        ContentValues accountValues = new ContentValues();
        UserClass user = UserClass.getInstance();

        accountValues.put("balance", balance);
        accountValues.put("useremail", user.getEmail());
        accountValues.put("payingability", payability.toString());
        db.insert("account", null, accountValues);

        accountsArraylist = getAccounts(user.getEmail());
        user.createAccount(accountsArraylist);
    }

    //Account whose number matches the given one gets updated with new payingability value
    //UserClasses accounts ArrayList is updated with new accounts so it includes ones with right values
    public void updateAccountInfo(Boolean hasPayingAbility, Integer accountNumber) {
        ContentValues accountValues = new ContentValues();
        accountValues.put("payingability", hasPayingAbility.toString());
        db.update("account", accountValues, "accountnumber=" + accountNumber, null);
        UserClass user = UserClass.getInstance();
        user.createAccount(getAccounts(user.getEmail()));
    }

    //Account whose number matches the given one is deleted from database
    //UserClasses accounts ArrayList is updated with new accounts so it doesn't include the deleted one
    public void  deleteAccount(Integer accountNumber) {
        db.delete("account", "accountnumber=" + accountNumber, null);
        UserClass user = UserClass.getInstance();
        user.createAccount(getAccounts(user.getEmail()));
    }

    //Account whose number matches the given one is deleted from database
    //(only for admin so UserClass update isn't needed)
    public void adminDeleteAccount(Integer accountNumber) {
        db.delete("account", "accountnumber=" + accountNumber, null);
    }

    //Gets all cards of account that has given accountnumber, returns them as ArrayList
    public ArrayList getCards(Integer accountNumber) {

        ArrayList<CardClass> cardsArraylist = new ArrayList<CardClass>();
        String selection = "accountnumber = ?";
        String[] selectionArgs = { accountNumber.toString() };
        String[] take = {"cardnumber", "paylimit", "withdrawlimit", "accountnumber"};
        Cursor cursor = db.query("card", take, selection, selectionArgs, null, null, null);

        while(cursor.moveToNext()) {
            Integer cnumber = cursor.getInt(cursor.getColumnIndex("cardnumber"));
            Float cpaylimit = cursor.getFloat(cursor.getColumnIndex("paylimit"));
            Float cwithdrawlimit = cursor.getFloat(cursor.getColumnIndex("withdrawlimit"));
            Integer anumber = cursor.getInt(cursor.getColumnIndex("accountnumber"));
            CardClass card = new CardClass(anumber, cnumber, cpaylimit, cwithdrawlimit);
            cardsArraylist.add(card);
        }
        cursor.close();

        return cardsArraylist;
    }

    //Adds new card to the database
    //AccountClass's cards ArrayList is updated with new cards so it includes the new one
    public void createNewCard(AccountClass account, Integer accountnumber, Float payingLimit, Float withdrawLimit) {

        ContentValues cardValues = new ContentValues();

        cardValues.put("paylimit", payingLimit);
        cardValues.put("withdrawlimit", withdrawLimit);
        cardValues.put("accountnumber", accountnumber);
        db.insert("card", null, cardValues);

        account.newCards(getCards(accountnumber));
    }

    //Card whose number matches the given one gets updated with new limits
    //AccountClass's cards ArrayList is updated with new cards so it includes ones with right values
    public void updateCardInfo(AccountClass cardsAccount, Float paylimit, Float withdrawlimit, Integer cardnumber) {
        ContentValues cardValues = new ContentValues();
        cardValues.put("paylimit", paylimit);
        cardValues.put("withdrawlimit", withdrawlimit);
        db.update("card", cardValues, "cardnumber=" + cardnumber, null);

        cardsAccount.newCards(getCards(cardsAccount.getAccountNumber()));
    }

    //Card whose number matches the given one is deleted from database
    //AccountClass's cards ArrayList is updated with new cards so it doesn't include the deleted one
    public void deleteCard(AccountClass account, Integer cardnumber) {
        db.delete("card", "cardnumber=" + cardnumber, null);
        account.newCards(getCards(account.getAccountNumber()));
    }

    //Card whose number matches the given one is deleted from database
    //(only for admin so AccountClass update isn't needed)
    public void adminDeleteCard(Integer cardnumber) {
        db.delete("card", "cardnumber=" + cardnumber, null);
    }

    //Gets all activities of account that has given accountnumber, returns them as ArrayList
    public ArrayList getActivities(Integer accountNumber){

        ArrayList<AccountActivityClass> activitiesArraylist = new ArrayList<AccountActivityClass>();
        String selection = "accountnumber = ?";
        String[] selectionArgs = { accountNumber.toString() };
        String[] take = {"activityid","date", "accountnumber", "activity"};
        Cursor cursor = db.query("accountactivity", take, selection, selectionArgs, null, null, null);

        while(cursor.moveToNext()) {
            Integer aid = cursor.getInt(cursor.getColumnIndex("activityid"));
            Date date = null;
            try {
                date = new SimpleDateFormat("dd.MM.yyyy").parse(cursor.getString(cursor.getColumnIndex("date")));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Integer accnumber = cursor.getInt(cursor.getColumnIndex("accountnumber"));
            String activity = cursor.getString(cursor.getColumnIndex("activity"));
            AccountActivityClass accActivity = new AccountActivityClass(aid, date, accnumber, activity);
            activitiesArraylist.add(accActivity);
        }
        cursor.close();

        return activitiesArraylist;
    }

    //Gets all accountactivities from the database, method is used to create xml file of them
    public ArrayList<AccountActivityClass> getAllActivities() {
        ArrayList<AccountActivityClass> activities = new ArrayList<AccountActivityClass>();

        String[] take = {"activityid","date", "accountnumber", "activity"};
        Cursor cursor = db.query("accountactivity", take, null, null, null, null, null);

        while(cursor.moveToNext()) {
            Integer aid = cursor.getInt(cursor.getColumnIndex("activityid"));
            Date date = null;
            try {
                date = new SimpleDateFormat("dd.MM.yyyy").parse(cursor.getString(cursor.getColumnIndex("date")));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Integer accnumber = cursor.getInt(cursor.getColumnIndex("accountnumber"));
            String activity = cursor.getString(cursor.getColumnIndex("activity"));
            AccountActivityClass accActivity = new AccountActivityClass(aid, date, accnumber, activity);
            activities.add(accActivity);
        }
        cursor.close();

        return activities;
    }

    //Adds new activity to the database
    //AccountClass's activities ArrayList is updated with new activities so it includes the new one
    public void createNewActivity(Integer accountnumber, String happenedActivity) {
        ContentValues activityValues = new ContentValues();
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");

        activityValues.put("accountnumber", accountnumber);
        activityValues.put("date", formatter.format(date));
        activityValues.put("activity", happenedActivity);
        db.insert("accountactivity", null, activityValues);

        //Remember to always update atm user's account's arraylist after using this method

    }

    //Is called from PaymentFragment to update the database with new balances to complete a payment between two accounts
    //Return Boolean, true if payment was made and false if payment wasn't made
    public Boolean makePayment(AccountClass account, Integer accountnumberfrom, Integer accountnumberto, Float amount) {

        //Gets balance from giving account
        String selection = "accountnumber = ?";
        String[] selectionArgs = { accountnumberfrom.toString() };
        String[] take = {"balance"};
        Cursor cursor = db.query("account", take, selection, selectionArgs, null, null, null);
        cursor.moveToNext();
        Float balanceFromAccount = Float.parseFloat(cursor.getString(0));
        cursor.close();

        //If theres not enough money on the account no transactions are made
        if(balanceFromAccount - amount < 0) {
            return false;
        }

        //Takes money from giving account and makes a new accountactivity of it
        ContentValues accountValues = new ContentValues();
        Float newBalance = balanceFromAccount - amount;
        accountValues.put("balance", newBalance);
        db.update("account", accountValues, "accountnumber=" + accountnumberfrom, null);
        String happenedActivity = "Payment -" + amount + "€";
        createNewActivity(accountnumberfrom, happenedActivity);

        //Tries to give the paid money to an account
        try {
            String[] selectionArgs2 = { accountnumberto.toString() };
            String[] take2 = {"balance"};
            Cursor cursor2 = db.query("account", take2, selection, selectionArgs2, null, null, null);
            cursor2.moveToNext();
            Float balanceInToAccount = cursor2.getFloat(cursor2.getColumnIndex("balance")) + amount;
            ContentValues accountValues2 = new ContentValues();
            accountValues2.put("balance", balanceInToAccount);
            db.update("account", accountValues2, "accountnumber=" + accountnumberto, null);
            cursor2.close();
            String happenedActivity2 = "Received payment +" + amount + "€";
            createNewActivity(accountnumberto, happenedActivity2);

        }catch(Exception e) {
            System.out.println("Recipient is outside this bank's database");

            //If we get here the given toAccount isn't in this database
            //This is where the money should be sent somewhere else (and recipient account number should then be something else than -1)
        }
        //UserClasses accounts ArrayList is updated with new accounts so it includes ones with right values
        UserClass user = UserClass.getInstance();
        user.createAccount(getAccounts(user.getEmail()));
        return true;
    }

    //Updates matching account's balance with new balance and makes a new activity with given String attribute
    //Used with CardPayment, Withdraw and AddMoney
    public void updateAccountBalance(Integer accountNumber, Float newBalance, AccountClass account, String happenedActivity) {
        ContentValues accountValues = new ContentValues();
        accountValues.put("balance", newBalance);
        db.update("account", accountValues, "accountnumber=" + accountNumber, null);
        createNewActivity(accountNumber, happenedActivity);
        //UserClasses accounts ArrayList is updated with new accounts so it includes ones with right values
        UserClass user = UserClass.getInstance();
        user.createAccount(getAccounts(user.getEmail()));
    }

    //Updates user and town tables that have user email matching current user's email
    public void saveUserInfosFromInputs(String givenFirstname, String givenLastname, Integer givenHouseNo, String givenStreet, Integer givenPostalcode, String givenCity, String givenPassword) {
        ContentValues userValues = new ContentValues();
        ContentValues townValues = new ContentValues();
        UserClass user = UserClass.getInstance();
        String email = user.getEmail();

        String selection = "useremail = ?";
        String[] selectionArgs = { email };
        townValues.put("postalcode", givenPostalcode);
        townValues.put("city", givenCity);
        db.update("town", townValues, selection, selectionArgs);

        String selection2 = "email = ?";
        userValues.put("firstname", givenFirstname);
        userValues.put("lastname", givenLastname);
        userValues.put("password", givenPassword);
        userValues.put("housenumber", givenHouseNo);
        userValues.put("street", givenStreet);
        userValues.put("postcode", givenPostalcode);
        db.update("user", userValues, selection2, selectionArgs);

        //Make userclass update its information with an ArrayList of values just gotten from the database
        user.updateUsersInfos(getUserInfos(email));
    }
}
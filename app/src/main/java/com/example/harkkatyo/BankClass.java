package com.example.harkkatyo;

import java.util.ArrayList;

class BankClass {
    private static BankClass single_instance = new BankClass();
    private DbManager db;
    private Integer bankId;
    private String bankName;

    public BankClass() {}

    public static BankClass getInstance() {
        if (single_instance == null)
            single_instance = new BankClass();

        return single_instance;
    }

    //Gets called from MainActivity to give Bank singleton it's values
    public void setBankValues() {
        db = DbManager.getInstance();
        ArrayList<String> bankValues;
        bankValues = db.getBank();
        bankId = Integer.parseInt(bankValues.get(0));
        bankName = bankValues.get(1);
    }

    public String getBankName() {return bankName;}
    public Integer getBankId() {return bankId;}

}

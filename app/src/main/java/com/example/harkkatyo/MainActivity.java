package com.example.harkkatyo;


import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

//MainActivity is the base for Login fragments in the application
//It has the container that is used to display the fragments related to Log in
public class MainActivity extends AppCompatActivity {

    private DbManager db;
    private Intent intent;
    private LoginFragment fragment;
    private CreateNewUserFragment fragment2;
    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;
    private UserClass userclass;

    //Opens database through DbManager
    //Makes singleton BankClass so the name is available for toolbar
    //In onCreate MainActivity makes possible existing UserClass Singleton null
    //Opens LoginFragment automatically when MainActivity is "called" or the app is opened
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        db = DbManager.getInstance();
        db.open(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        BankClass bank = BankClass.getInstance();
        bank.setBankValues();
        getSupportActionBar().setTitle(bank.getBankName());

        UserClass.nullUser();

        showFragment(new LoginFragment());
    }

    //Opens CerateNewUserFragment when CreateUser button is pressed in LoginFragment
    public void CreateUserFromLogIn (View v) {
        showFragment(new CreateNewUserFragment());
    }

    //Opens next activity (=Logs in) when Log in button is pressed in LoginFragment
    //Next Activity is chosen according to the String that method correctLogin returns
    public void LogInFromLogIn(View v) {
        fragmentManager = getSupportFragmentManager();
        fragment = (LoginFragment) fragmentManager.findFragmentById(R.id.fragments);
        String fromLoginFragment = fragment.correctLogin();

        if (fromLoginFragment.equals("user")) {
            intent = new Intent(this, BaseActivity.class);
            startActivity(intent);
            finish();
        }else if (fromLoginFragment.equals("admin")) {
            intent = new Intent(this, AdminActivity.class);
            startActivity(intent);
            finish();
        }
    }

    //When Register button is pressed in CreateNewUserFragment calls method registerUserFromInputs to gather the given inputs
    //Opens LoginFragment if register information was given right (method returned string "true")
    public void registerFromRegister(View v) {
        fragmentManager = getSupportFragmentManager();
        fragment2 = (CreateNewUserFragment) fragmentManager.findFragmentById(R.id.fragments);
        String fromFragment = fragment2.registerUserFromInputs();

        if (fromFragment.equals("true")) {
            showFragment(new LoginFragment());
        }
    }

    //Opens LoginFragment when Back button is pressed in CreateNewUserFragment
    //Replaces normal onBackPressed method
    public void getBackFromRegister(View v) {
        showFragment(new LoginFragment());
    }

    private void showFragment(Fragment wantedFragment) {
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragments, wantedFragment);
        fragmentTransaction.commit();
    }

    //Normal backPress doesn't work to prevent getting back to createNewUserFragment
    @Override
    public void onBackPressed() {
        System.out.println("Back press not allowed. Use the buttons in the app");
    }
}

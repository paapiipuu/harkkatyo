package com.example.harkkatyo;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

public class CreateNewCardFragment extends Fragment {

    private View  view;
    private EditText paylimit, withdrawlimit;
    private TextView errorMessage;
    private DbManager db;
    private Float payLimit, withdrawLimit;
    private AccountClass account;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        Bundle bundle = this.getArguments();
        account = (AccountClass) bundle.getSerializable("keyToCardsAccount");
        view = inflater.inflate(R.layout.fragment_createnewcard, container, false);
        paylimit = view.findViewById(R.id.editText5);
        withdrawlimit = view.findViewById(R.id.editText6);
        errorMessage = view.findViewById(R.id.errormessage);
        return view;
    }

    //Calls DbManager method to add new card to database with inputs given by user
    //Returns String "yes" if create was successful
    public String createNewCardFromInputs() {
        errorMessage.setVisibility(View.INVISIBLE);
        db = DbManager.getInstance();
        String toActivity = "yes";

        try {
            payLimit = Float.parseFloat(paylimit.getText().toString());
            withdrawLimit = Float.parseFloat(withdrawlimit.getText().toString());
            if(payLimit < 0 || withdrawLimit < 0) {
                errorMessage.setVisibility(View.VISIBLE);
                toActivity = "no";
            }else{
                db.createNewCard(account, account.getAccountNumber(), payLimit, withdrawLimit);
            }
        }catch(Exception e) {
            //getting here means values couldn't be parsed to Floats
            errorMessage.setVisibility(View.VISIBLE);
            toActivity = "no";
        }
        return toActivity;
    }

    public AccountClass getShownAccount() {
        return account;
    }
}

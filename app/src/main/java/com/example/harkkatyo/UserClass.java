package com.example.harkkatyo;

import android.accounts.Account;

import java.util.ArrayList;

public class UserClass {

    private String userFirstName, userLastName, userPassword, userStreet, userTown;
    private Integer userBankId, userHouseNo, userPostcode;
    private DbManager db;
    private static UserClass userclass = null;
    private final String userEmail;
    private ArrayList<AccountClass> accounts;

    private UserClass(String email) {
        this.userEmail = email;
        db = DbManager.getInstance();
        ArrayList<String> infos;

        infos = db.getUserInfos(userEmail);
        userBankId = Integer.parseInt(infos.get(0));
        userFirstName = infos.get(1);
        userLastName = infos.get(2);
        userPassword = infos.get(3);
        userHouseNo = Integer.parseInt(infos.get(4));
        userStreet = infos.get(5);
        userPostcode = Integer.parseInt(infos.get(6));
        userTown = infos.get(7);

        accounts = db.getAccounts(userEmail);
    }

    public static UserClass getInstance() {
        if(userclass == null) {
            throw new AssertionError("You have to call init first");
        }
        return userclass;
    }

    //Method that is called to initialize UserClass
    //Makes itself with given email in in constructor
    public synchronized static UserClass init(String email) {
        if(userclass != null) {
            throw new AssertionError("You have already initialized me");
        }
        userclass = new UserClass(email);
        return userclass;
    }

    public String getEmail() {return userEmail;}
    public String getUserFirstName() {return userFirstName;}
    public String getUserLastName() {return userLastName;}
    public Integer getUserHouseNo() {return userHouseNo;}
    public String getUserStreet() {return userStreet;}
    public Integer getUserPostcode() {return userPostcode;}
    public String getUserTown() {return userTown;}
    public String getUserPassword() {return userPassword;}

    //DbManager calls this when new account is added to or deleted from the database
    //Old arraylist is emptied and new one is made
    public void createAccount(ArrayList<AccountClass> list) {
        accounts.clear();
        accounts = list;
    }

    public ArrayList<AccountClass> getAccounts () {
        return accounts;
    }

    //Returns accounts that have paying ability as ArrayList
    public ArrayList<AccountClass> getAccountsWithPayAbility () {
        ArrayList<AccountClass> accountsWithPayAbility = new ArrayList<AccountClass>();

        for(AccountClass oneOfAccounts : accounts) {
            if (oneOfAccounts.getPayingAbility() == true) {
                accountsWithPayAbility.add(oneOfAccounts);
            }
        }
        return accountsWithPayAbility;
    }

    //Updates the attributes of the user
    public void updateUsersInfos(ArrayList<String> infos) {

        userBankId = Integer.parseInt(infos.get(0));
        userFirstName = infos.get(1);
        userLastName = infos.get(2);
        userPassword = infos.get(3);
        userHouseNo = Integer.parseInt(infos.get(4));
        userStreet = infos.get(5);
        userPostcode = Integer.parseInt(infos.get(6));
        userTown = infos.get(7);
    }

    public void nullAccountsWhenLoginOut() {
        if (accounts != null) {
            accounts.clear();
        }
    }

    //Called in MainActivity so UserClass can certainly be initialized again with new email in Log In
    public static void nullUser() {
        if (userclass != null) {
            userclass = null;
        }
    }
}
package com.example.harkkatyo;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class CardPaymentFragment extends Fragment {

    private View view;
    private UserClass user;
    private TextView errorBox;
    private EditText amountEdit;
    private String noAccount="No account chosen", noCard="No card given", illegalAmount="Amount was not given right", lowerPaymentLimit="Card's payment limit prevents payment", notEnoughMoney="Chosen account doesn't have  enough money";
    private Spinner accountsSpinner, cardsSpinner;
    private AccountClass account = null;
    private CardClass card = null;
    private Float amount;
    private DbManager db;

    //Layout consists of paying account spinner, card spinner, and amount text box

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_cardpayment, container, false);
        user = UserClass.getInstance();
        errorBox = view.findViewById(R.id.errormessage3);
        errorBox.setText("");
        amountEdit = view.findViewById(R.id.editText17);
        db = DbManager.getInstance();

        accountsSpinner = (Spinner) view.findViewById(R.id.spinner6);
        final ArrayAdapter<AccountClass> adapter = new ArrayAdapter<AccountClass>(getContext(), android.R.layout.simple_spinner_item, user.getAccountsWithPayAbility());
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        accountsSpinner.setAdapter(adapter);
        accountsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            //When an account is chosen next spinner displays its cards
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {

                errorBox.setText("");
                account = adapter.getItem(position);

                final ArrayAdapter<CardClass> adapter2 = new ArrayAdapter<CardClass>(getContext(), android.R.layout.simple_spinner_item, account.getCards());
                adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                cardsSpinner.setAdapter(adapter2);
                cardsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {

                        errorBox.setText("");
                        card = adapter2.getItem(position);

                    }
                    @Override
                    public void onNothingSelected(AdapterView<?> adapter) { card = null; }
                });

            }
            @Override
            public void onNothingSelected(AdapterView<?> adapter) { account = null;  }
        });

        cardsSpinner = (Spinner) view.findViewById(R.id.spinner7);

        return view;
    }

    //Gets called from the BaseActivity to make a balance update with the DbManager with given inputs
    //Return true if was able to make withdraw and false if something prevented the payment from being done
    public Boolean makePayment () {
        errorBox.setText("");
        if(account == null) {
            //no account was given
            errorBox.setText(noAccount);
            return false;
        }else if (card == null) {
            //no card was given
            errorBox.setText(noCard);
            return false;
        }
        try {
            amount = Float.parseFloat(amountEdit.getText().toString());
            if(amount < 0) {
                errorBox.setText(illegalAmount);
                return false;
            }
        }catch(Exception e) {
            //wasn't able to parse input to float
            errorBox.setText(illegalAmount);
            return false;
        }
        if(amount > card.getpayLimit()) {
            errorBox.setText(lowerPaymentLimit);
            return false;
        }
        if(amount > account.getBalance()) {
            errorBox.setText(notEnoughMoney);
            return false;
        }

        //If we get here or inputs are given right and payment amount isn't too big for the card or the account
        NumberFormat formatter = new DecimalFormat("00000000");
        String happenedActivity = "Payment -" + amount + "€ from card " + formatter.format(card.getcardNumber()).toString();
        db.updateAccountBalance(account.getAccountNumber(), (account.getBalance() - amount), account, happenedActivity);

        //Stimulated card payment
        System.out.println("Paid " + amount + "€");
        return true;
    }
}

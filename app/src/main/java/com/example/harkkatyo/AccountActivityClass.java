package com.example.harkkatyo;

import java.text.SimpleDateFormat;
import java.util.Date;

public class AccountActivityClass {

    private Integer activityid, accountnumber;
    private String activity;
    private Date date;

    public AccountActivityClass (Integer activityId, Date dateFrom, Integer acccoundNumber, String Activity) {
        activityid = activityId;
        accountnumber = acccoundNumber;
        activity = Activity;
        date = dateFrom;
    }

    public Integer getActivityId() { return activityid; }
    public Integer getAccountnumber() { return accountnumber; }
    public String getActivity() { return activity; }

    public String getDateSTR() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
        return formatter.format(date);
    }

    @Override
    public String toString() {
        return activity;
    }
}

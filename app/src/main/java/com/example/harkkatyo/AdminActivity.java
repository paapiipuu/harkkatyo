package com.example.harkkatyo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

//AdminActivity is the base for all Admin's fragments
//It has the container that is used to display the fragments related to Admin
public class AdminActivity extends AppCompatActivity {

    private Fragment fragment;
    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        BankClass bank = BankClass.getInstance();
        getSupportActionBar().setTitle(bank.getBankName());

        //Shows AdminViewFragment automatically when this activity is created
        showFragment(new AdminViewFragment());
    }

    //When Show User button is pressed in AdminViewFragment and a user is chosen method opens
    //new AdminShowUserFragment with chosen user's email
    public void adminShowUser(View v) {
        fragmentManager = getSupportFragmentManager();
        AdminViewFragment adminviewfragment = (AdminViewFragment) fragmentManager.findFragmentById(R.id.fragmentWindow2);
        String userEmail = adminviewfragment.showChosenUser();
        if(userEmail != null) {
            Bundle bundle = new Bundle();
            bundle.putString("email" , userEmail);
            showFragmentWithBundle(bundle, new AdminShowUserFragment());
        }
    }

    //When Delete User button is pressed in AdminViewFragment and a user is chosen method calls
    //fragment method to delete this user and if delete is successful "updates" the view
    public void adminDeleteUser(View v) {
        fragmentManager = getSupportFragmentManager();
        AdminViewFragment adminviewfragment = (AdminViewFragment) fragmentManager.findFragmentById(R.id.fragmentWindow2);
        Boolean deleteWasMade = adminviewfragment.deleteChosenUser();
        if(deleteWasMade == true) {
            showFragment(new AdminViewFragment());
        }
    }

    //When Show Account button is pressed in AdminViewFragment and ana account is chosen method opens
    //new AdminShowAccountFragment with chosen account
    public void adminShowAccount(View v) {
        fragmentManager = getSupportFragmentManager();
        AdminViewFragment adminviewfragment = (AdminViewFragment) fragmentManager.findFragmentById(R.id.fragmentWindow2);
        AccountClass account= adminviewfragment.showChosenAccount();
        if(account != null) {
            Bundle bundle = new Bundle();
            bundle.putSerializable("account" , account);
            showFragmentWithBundle(bundle, new AdminShowAccountFragment());
        }
    }

    //When Delete Account button is pressed in AdminViewFragment and an account is chosen method calls
    //fragment method to delete this account and if delete is successful "updates" the view
    public void adminDeleteAccount(View v) {
        fragmentManager = getSupportFragmentManager();
        AdminViewFragment adminviewfragment = (AdminViewFragment) fragmentManager.findFragmentById(R.id.fragmentWindow2);
        Boolean deleteWasMade = adminviewfragment.deleteChosenAccount();
        if(deleteWasMade == true) {
            showFragment(new AdminViewFragment());
        }
    }

    //When Show Card button is pressed in AdminViewFragment and a card is chosen method opens
    //new AdminShowCardFragment with chosen card
    public void adminShowCard(View v) {
        fragmentManager = getSupportFragmentManager();
        AdminViewFragment adminviewfragment = (AdminViewFragment) fragmentManager.findFragmentById(R.id.fragmentWindow2);
        CardClass card= adminviewfragment.showChosenCard();
        if(card != null) {
            Bundle bundle = new Bundle();
            bundle.putSerializable("card" , card);
            showFragmentWithBundle(bundle, new AdminShowCardFragment());
        }
    }

    //When Delete Card button is pressed in AdminViewFragment and a card is chosen method calls
    //fragment method to delete this card and if delete is successful "updates" the view
    public void adminDeleteCard(View v) {
        fragmentManager = getSupportFragmentManager();
        AdminViewFragment adminviewfragment = (AdminViewFragment) fragmentManager.findFragmentById(R.id.fragmentWindow2);
        Boolean deleteWasMade = adminviewfragment.deleteChosenCard();
        if(deleteWasMade == true) {
            showFragment(new AdminViewFragment());
        }

    }

    //Logs admin out ( = starts MainActivity) when Log Out button in AdminViewFragment is pressed
    public void adminLogOut(View v) {
        this.finish();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    //Used in AdminShow fragments to get back to AdminViewFragment
    public void adminGetBack(View v) {
        showFragment(new AdminViewFragment());
    }

    private void showFragmentWithBundle(Bundle bundle, Fragment wantedFragment) {
        wantedFragment.setArguments(bundle);
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragmentWindow2, wantedFragment);
        fragmentTransaction.commit();
    }

    private void showFragment(Fragment wantedFragment) {
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragmentWindow2, wantedFragment);
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {
        System.out.println("Back press not allowed. Use the buttons in the app");
    }

}

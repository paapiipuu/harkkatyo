package com.example.harkkatyo;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class AddMoneyFragment extends Fragment {
    private View view;
    private UserClass user;
    private TextView errorBox;
    private EditText amountEdit;
    private String noAccount="No account chosen", illegalAmount="Amount was not given right";
    private Spinner accountsSpinner;
    private AccountClass account = null;
    private Float amount;
    private DbManager db;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_addmoney, container, false);
        user = UserClass.getInstance();
        errorBox = view.findViewById(R.id.errormessage5);
        errorBox.setText("");
        amountEdit = view.findViewById(R.id.editText18);
        db = DbManager.getInstance();

        accountsSpinner = (Spinner) view.findViewById(R.id.spinner8);
        final ArrayAdapter<AccountClass> adapter = new ArrayAdapter<AccountClass>(getContext(), android.R.layout.simple_spinner_item, user.getAccounts());
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        accountsSpinner.setAdapter(adapter);
        accountsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {

                errorBox.setText("");
                account = adapter.getItem(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) { account = null; }
        });
        return view;
    }

    //Gets called from BaseActivity to add wanted amount of money to chosen account
    //Returns true if values are given in right types and money is added to the account
    public Boolean addMoney() {
        errorBox.setText("");
        if(account == null) {
            errorBox.setText(noAccount);
            return false;
        }
        try {
            amount = Float.parseFloat(amountEdit.getText().toString());
            if(amount < 0) {
                errorBox.setText(illegalAmount);
                return false;
            }
        }catch(Exception e) {
            errorBox.setText(illegalAmount);
            return false;
        }
        //If we get here all inputs wer given right
        String happenedActivity = "Money added " + amount + "€";
        db.updateAccountBalance(account.getAccountNumber(), (account.getBalance() + amount), account, happenedActivity);
        return true;
    }
}

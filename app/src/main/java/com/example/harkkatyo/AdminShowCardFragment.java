package com.example.harkkatyo;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.NumberFormat;

//80, 82, 84, 64

public class AdminShowCardFragment extends Fragment {

    private View view;
    private CardClass card;
    private TextView cardNumber, accountNumber, payLimit, withdrawLimit;

    //Gets card's information from it and displays it in right formats
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_showcard, container, false);
        Bundle bundle = this.getArguments();
        card = (CardClass) bundle.getSerializable("card");

        cardNumber = view.findViewById(R.id.textView80);
        NumberFormat formatter = new DecimalFormat("00000000");
        cardNumber.setText(formatter.format(card.getcardNumber()).toString());
        accountNumber = view.findViewById(R.id.textView82);
        accountNumber.setText("FI" +formatter.format(card.getAccountNumber()).toString());
        payLimit = view.findViewById(R.id.textView84);
        payLimit.setText(card.getpayLimit().toString() + "€");
        withdrawLimit = view.findViewById(R.id.textView64);
        withdrawLimit.setText(card.getwithdrawLimit().toString() + "€");

        return view;
    }
}


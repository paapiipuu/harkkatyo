package com.example.harkkatyo;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;

//RecyclerView for displaying users's accounts
public class recyclerViewClass extends RecyclerView.Adapter<recyclerViewClass.ViewHolder> {


    private static final String TAG = "recyclerViewClass";
    private ArrayList<AccountClass> accounts;
    private Context mContext;
    private OnAccountListener onAccountListener;
    private Integer positionOfClick = RecyclerView.NO_POSITION;

    public recyclerViewClass(Context context, ArrayList<AccountClass> accountsArray, OnAccountListener onaccountlistener) {
        mContext = context;
        accounts = accountsArray;
        this.onAccountListener = onaccountlistener;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View w = LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem, parent, false);
        ViewHolder holder = new ViewHolder(w);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder Viewholder, final int position) {
        NumberFormat formatter = new DecimalFormat("00000000");
        Viewholder.accountNumber.setText("FI" + formatter.format(accounts.get(position).getAccountNumber()).toString());
        DecimalFormat df = new DecimalFormat("0.00");
        Viewholder.accountBalance.setText(df.format(accounts.get(position).getBalance()).toString() + "€");

        Viewholder.parent_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                positionOfClick = position;

                onAccountListener.onAccountClick(position);
                notifyDataSetChanged();
            }
        });

        //Making chosen account highlighted
        if(positionOfClick == position){
            Viewholder.parent_layout.setBackgroundColor(Color.parseColor("#FFC9C5C9"));
        }
        else{
            Viewholder.parent_layout.setBackgroundColor(Color.parseColor("#ffffff"));
        }
    }

    @Override
    public int getItemCount() {
        return accounts.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder{

        RelativeLayout parent_layout;
        public TextView accountNumber, accountBalance;

        public ViewHolder(View itemView) {
            super(itemView);
            parent_layout = itemView.findViewById(R.id.parentLayout);
            accountNumber = (TextView) itemView.findViewById(R.id.accountnumberToLayout);
            accountBalance = (TextView) itemView.findViewById(R.id.balanceToLayout);
        }
    }

    //Interface that AccountsFragment implements to help with getting chosen account
    public interface OnAccountListener {
        void onAccountClick(int position);

    }
}
package com.example.harkkatyo;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

public class UserInformationFragment extends Fragment {

    private View view;
    private UserClass user;
    private DbManager db;
    private EditText  userFirstName, userLastName, userHouseNo, userStreet, userPostalCode, userTown, userPassword;
    private TextView errorMessage;
    private String errorWrongInts = "Numeric values were given wrong", errorNotAllInfos = "You didn't give all the required information";


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_userinformation, container, false);
        user = UserClass.getInstance();
        db = DbManager.getInstance();


        errorMessage = view.findViewById(R.id.errorMessageUserInfo);
        errorMessage.setText("");
        userFirstName = view.findViewById(R.id.editText25);
        userFirstName.setText(user.getUserFirstName());
        userLastName = view.findViewById(R.id.editText26);
        userLastName.setText(user.getUserLastName());
        userHouseNo = view.findViewById(R.id.editText27);
        userHouseNo.setText(user.getUserHouseNo().toString());
        userStreet = view.findViewById(R.id.editText28);
        userStreet.setText(user.getUserStreet());
        userPostalCode = view.findViewById(R.id.editText19);
        userPostalCode.setText(user.getUserPostcode().toString());
        userTown = view.findViewById(R.id.editText20);
        userTown.setText(user.getUserTown());
        userPassword = view.findViewById(R.id.editText21);
        userPassword.setText(user.getUserPassword());

        return view;
    }

    //BaseActivity calls this to take given inputs and forward them to the database to update user's information when Save button is pressed
    //Returns false if save didn't happen because of wrong inputs
    public Boolean saveUserInputs() {
        errorMessage.setText("");
        if (userFirstName.getText().toString().isEmpty() || userLastName.getText().toString().isEmpty() || userHouseNo.getText().toString().isEmpty() || userStreet.getText().toString().isEmpty() || userPostalCode.getText().toString().isEmpty() || userTown.getText().toString().isEmpty() || userPassword.getText().toString().isEmpty()) {
            //One of the text boxes was edited but left empty
            errorMessage.setText(errorNotAllInfos);
            return false;
        }
        try {
            Integer.parseInt(userHouseNo.getText().toString());
            Integer.parseInt(userPostalCode.getText().toString());
        }catch(Exception e){
            //User inputs that ares supposed to be integers were given wrong
            errorMessage.setText(errorWrongInts);
            return false;
        }
        //All user infos were given correctly
        db.saveUserInfosFromInputs(userFirstName.getText().toString(), userLastName.getText().toString(), Integer.parseInt(userHouseNo.getText().toString()), userStreet.getText().toString(),
                Integer.parseInt(userPostalCode.getText().toString()), userTown.getText().toString(), userPassword.getText().toString());
        System.out.println("@@@@@@@@@@@@@@@@@@@@User information was updated");
        return true;
    }
}

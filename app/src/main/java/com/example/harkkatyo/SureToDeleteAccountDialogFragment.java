package com.example.harkkatyo;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

public class SureToDeleteAccountDialogFragment extends DialogFragment {

    public interface AlertDialogListener {
        void onDialogPositiveClick(DialogFragment dialog);
        void onDialogNegativeClick(DialogFragment dialog);
    }

    AlertDialogListener listener;

    //Makes sure DialogFragment's user is implementing AlertDialogListener
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            listener = (AlertDialogListener) context;
        }catch(Exception e) {
            throw new ClassCastException(getActivity().toString() + "must implement AlertDialogListener");
        }
    }

    //Layout and message for pop up
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Are You sure you want to delete this account now? The remaining money on the account will be sent to you in cash.")
                .setPositiveButton("DELETE", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if(listener != null)
                            listener.onDialogPositiveClick(SureToDeleteAccountDialogFragment.this);
                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if(listener != null)
                            listener.onDialogNegativeClick(SureToDeleteAccountDialogFragment.this);
                    }
                });
        return builder.create();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }
}

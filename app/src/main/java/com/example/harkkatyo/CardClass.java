package com.example.harkkatyo;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.NumberFormat;

public class CardClass implements Serializable {

    private Float payLimit, withdrawLimit;
    private Integer accountNumber, cardNumber;

    public CardClass(Integer accountnumber, Integer cardnumber, Float paylimit, Float withdrawlimit) {
        accountNumber = accountnumber;
        cardNumber = cardnumber;
        payLimit = paylimit;
        withdrawLimit = withdrawlimit;
    }

    public Integer getcardNumber() { return cardNumber; }
    public Integer getAccountNumber() {return accountNumber;}
    public Float getpayLimit() { return payLimit; }
    public Float getwithdrawLimit() { return withdrawLimit; }

    //Overriding toString function adapter uses when printing the cards to spinner
    @Override
    public String toString() {
        NumberFormat formatter = new DecimalFormat("00000000");
        return (formatter.format(cardNumber));
    }
}

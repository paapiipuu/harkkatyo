package com.example.harkkatyo;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.NumberFormat;

// 87, 91, 93, 89

public class AdminShowAccountFragment extends Fragment {

    private View view;
    private AccountClass account;
    private TextView accountNumber, userEmail, balance, payingAbility;

    //Gets account's information from it and displays it in right formats
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_showaccount, container, false);
        Bundle bundle = this.getArguments();
        account = (AccountClass) bundle.getSerializable("account");
        accountNumber = view.findViewById(R.id.textView87);
        NumberFormat formatter = new DecimalFormat("00000000");
        accountNumber.setText("FI" + formatter.format(account.getAccountNumber()).toString());
        userEmail = view.findViewById(R.id.textView91);
        userEmail.setText(account.getUserEmail());
        balance = view.findViewById(R.id.textView93);
        balance.setText(account.getBalance().toString() + "€");
        payingAbility = view.findViewById(R.id.textView89);
        payingAbility.setText(account.getPayingAbility().toString());

        return view;
    }
}

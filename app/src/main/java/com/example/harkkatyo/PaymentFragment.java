package com.example.harkkatyo;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class PaymentFragment extends Fragment {

    private View view;
    private UserClass user;
    private Spinner fromAccountsSpinner, toAccountsSpinner;
    private EditText toUnknownAccountEdit, amountEdit;
    private AccountClass accountPay = null, accountRecipient = null;
    private Float amount;
    private DbManager db;
    private TextView errorBox;
    private String noFrom = "No account chosen", noTo = "No recipient given", illegalAmount = "Amount was not given right", notEnoughMoney = "Chosen account doesn't have enough money";

    //Layout consists of paying account spinner, recipient account spinner, input box for recipient account number, box for amount

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_payment, container, false);
        user = UserClass.getInstance();
        db = DbManager.getInstance();

        toUnknownAccountEdit = view.findViewById(R.id.editText14);
        toUnknownAccountEdit.setText("");
        amountEdit = view.findViewById(R.id.editText15);
        errorBox = view.findViewById(R.id.errormessage4);
        errorBox.setText("");

        fromAccountsSpinner = (Spinner) view.findViewById(R.id.spinner3);
        final ArrayAdapter<AccountClass> adapter = new ArrayAdapter<AccountClass>(getContext(), android.R.layout.simple_spinner_item, user.getAccountsWithPayAbility());
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        fromAccountsSpinner.setAdapter(adapter);
        fromAccountsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {

                errorBox.setText("");
                accountPay = adapter.getItem(position);

            }
            @Override
            public void onNothingSelected(AdapterView<?> adapter) { accountPay = null;  }
        });

        toAccountsSpinner = (Spinner) view.findViewById(R.id.spinner2);
        final ArrayAdapter<AccountClass> adapter2 = new ArrayAdapter<AccountClass>(getContext(), android.R.layout.simple_spinner_item, user.getAccounts());
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        toAccountsSpinner.setAdapter(adapter2);
        toAccountsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {

                toUnknownAccountEdit.setText("", TextView.BufferType.EDITABLE);
                errorBox.setText("");
                accountRecipient = adapter2.getItem(position);

            }
            @Override
            public void onNothingSelected(AdapterView<?> adapter) { accountRecipient = null;  }
        });

        return view;
    }

    //Gets called from BaseActivity when Pay button is pressed
    //Interacts with DbManager to make a payment
    //Return true if payment was made, otherwise returns false
    public Boolean makePayment() {
        Boolean updateWasMade;
        errorBox.setText("");
        if (accountPay == null) {
            //No paying account given, errormessage, stop progress
            errorBox.setText(noFrom);
            return false;

        }else if(toUnknownAccountEdit.getText().toString().equals("")) {
            //No edittext account given
            if(accountRecipient == null) {
                //No recipient account given, error message, stop progress
                errorBox.setText(noTo);
                return false;
            }
            try {
                amount = Float.parseFloat(amountEdit.getText().toString());
                if(amount < 0) {
                    errorBox.setText(illegalAmount);
                    return false;
                }
            }catch (Exception e) {
                //Wasn't able to parse given value to Float, amount was given wrong, error message, stop progress
                errorBox.setText(illegalAmount);
                return false;
            }
            //All info is given right and recipient account is chosen from the spinner -> it's user's own
            //If chosen paying account has enough money UpdateWasMAde is true, otherwise it's false and payment wasn't completed
            updateWasMade = db.makePayment(accountPay, accountPay.getAccountNumber(), accountRecipient.getAccountNumber(), amount);
            if(updateWasMade == false) {errorBox.setText(notEnoughMoney);}
            return updateWasMade;
        }
        try {
            amount = Float.parseFloat(amountEdit.getText().toString());
        }catch (Exception e) {
            //Wasn't able to parse given value to Float, amount was given wrong, error message, stop progress
            errorBox.setText(illegalAmount);
            return false;
        }
        //Checked that paying account, recipient account and amount are okay values
        try {
            // Splitting the string to numbers and Country part (FI00000003 -> F | 00000003)
            String[] parts = toUnknownAccountEdit.getText().toString().split("I");
            String part1 = parts[0];
            String part2 = parts[1];
            Integer accountNumberTo = Integer.parseInt(part2);
            updateWasMade = db.makePayment(accountPay, accountPay.getAccountNumber(), accountNumberTo, amount);
        }catch (Exception e) {
            //If catches accountnumber is not in this banks format and payment will be handled with that in mind
            //Account number is set to -1, because it doesn't follow this banks format, and so we can't parse it to be required Integer
            //If we had a possibility to transfer money to another bank's system this is where it should be done instead of defaulting the account number to make the method work
            updateWasMade = db.makePayment(accountPay, accountPay.getAccountNumber(), -1, amount);
        }
        if(updateWasMade == false) {errorBox.setText(notEnoughMoney);}
        return updateWasMade;
    }

}
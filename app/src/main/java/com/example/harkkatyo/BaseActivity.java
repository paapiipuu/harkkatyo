package com.example.harkkatyo;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;

//TODO DO WHEN BREAK IS NEEDED Check that all textboxes are big enough for texts

//TODO DO WHEN EVERYTHING ELSE IS STUPID Put all strings to String file -> no hardcode strings, do by using FIX

//BaseActivity is called from MainActivity when a normal user logs in
//It has the container that is used to display the fragments related to normal user's bank functions
public class BaseActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, SureToDeleteAccountDialogFragment.AlertDialogListener, popUpDialogFragment.AlertDialogListener{

    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;
    private DrawerLayout mLayout;
    private ActionBarDrawerToggle mToggle;
    private AccountClass account, accountThatWasBeforeGoingToCreateNewCard;

    //Opens AccountsFragment automatically when BaseActivity is "called"
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        BankClass bank = BankClass.getInstance();
        getSupportActionBar().setTitle(bank.getBankName());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //open and close in values strings file
        mToggle = new ActionBarDrawerToggle(this, mLayout, R.string.open, R.string.close);

        mLayout.addDrawerListener(mToggle);
        mToggle.syncState();

        showFragment(new AccountsFragment());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId() == android.R.id.home){
            mLayout.openDrawer(Gravity.LEFT);
        }
        return super.onOptionsItemSelected(item);
    }

    //Method that controls what happens when an option is pressed from the side menu
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        switch(menuItem.getItemId()) {

            case R.id.nav_logout:
                //Write xml file of all accountactivities
                WriteXMLClass xml = WriteXMLClass.getInstance();
                xml.writeFile(this);
                //Nulls user's accounts before logging out and then starts new MainActivity as Loging out
                UserClass user = UserClass.getInstance();
                fragmentManager = getSupportFragmentManager();
                this.finish();
                user.nullAccountsWhenLoginOut();
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                finish();
            case R.id.nav_accounts:
                showFragment(new AccountsFragment());
                break;
            case R.id.nav_userinfo:
                showFragment(new UserInformationFragment());
                break;
            case R.id.nav_payment:
                showFragment(new PaymentFragment());
                break;
            case R.id.nav_withdraw:
                showFragment(new WithdrawFragment());
                break;
            case R.id.nav_card_payment:
                showFragment(new CardPaymentFragment());
                break;
            case R.id.nav_addmoney:
                showFragment(new AddMoneyFragment());
                break;
        }
        return true;
    }

    //Gets called to open a fragment to create an account when CreateNewAccount button is pressed in AccountsFragment
    public void createAccount (View v) {
        showFragment(new CreateNewAccountFragment());
    }

    //Is called from CreateNewAccountFragment and EditAccountFragment
    public void getBackToAccounts (View v) {
        showFragment(new AccountsFragment());
    }

    //Gets called to create new account with fragment method and get back to AccountFragment if create was successful when
    //Add button is pressed in CreateNewAccountFragment
    public void addCreatedAccount (View v) {
        fragmentManager = getSupportFragmentManager();
        CreateNewAccountFragment createnewaccountfragment = (CreateNewAccountFragment) fragmentManager.findFragmentById(R.id.fragmentWindow2);
        String wasComplete = createnewaccountfragment.createNewAccountFromInputs();

        if(wasComplete.equals("yes")) {
            showFragment(new AccountsFragment());
        }
    }

    //When Show Account button is pressed in AccountsFragment and an account is chosen method opens
    //new EditAccountFragment with this chosen account and makes it an attribute for other methods to use
    public void showAccount (View v) {
        Bundle bundle = new Bundle();
        fragmentManager = getSupportFragmentManager();
        AccountsFragment accountsfragment = (AccountsFragment) fragmentManager.findFragmentById(R.id.fragmentWindow2);
        account = accountsfragment.getChosenAccount();
        accountsfragment.makeAccountNull();
        accountThatWasBeforeGoingToCreateNewCard = account;
        if (account != null){
            bundle.putSerializable("keyToChosenAccount" , account);
            showFragmentWithBundle(bundle, new EditAccountFragment());
        }
    }

    //When Delete button is pressed in AccountsFragment and an account is chosen method displays a sureToDelete pop up
    public void deleteAccount(View v) {
        fragmentManager = getSupportFragmentManager();
        AccountsFragment accountsfragment = (AccountsFragment) fragmentManager.findFragmentById(R.id.fragmentWindow2);
        if(accountsfragment.getChosenAccount() != null) {
            DialogFragment sureToDelete = new SureToDeleteAccountDialogFragment();
            sureToDelete.show(fragmentManager, "theDelete Dialog");
        }
    }

    //User selected delete option on pop up so method calls another to delete the chosen account
    //View is "updated" so deleted account doesn't show there
    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        fragmentManager = getSupportFragmentManager();
        AccountsFragment accountsfragment = (AccountsFragment) fragmentManager.findFragmentById(R.id.fragmentWindow2);
        Boolean didDeleteHappen = accountsfragment.deleteChosenAccount();
        if(didDeleteHappen == true) {
            showFragment(new AccountsFragment());
        }
    }

    //User canceled delete and pop up hides automatically on click
    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
    }

    //When Create Card button is pressed in EditAccountFragment opens CreateNewCardFragment and gives
    //it the displayed account as bundle so the card can be attached to this account
    public void createNewCard(View v) {
        Bundle bundle = new Bundle();
        fragmentManager = getSupportFragmentManager();
        bundle.putSerializable("keyToCardsAccount" , accountThatWasBeforeGoingToCreateNewCard);
        showFragmentWithBundle(bundle, new CreateNewCardFragment());
    }

    //Gets called to create new card with fragment method and get back to EditAccountFragment if create was successful when
    //Add button is pressed in CreateNewCardFragment
    public void addCreatedCard(View v) {
        Bundle bundle = new Bundle();
        fragmentManager = getSupportFragmentManager();
        CreateNewCardFragment createnewcardfragment = (CreateNewCardFragment) fragmentManager.findFragmentById(R.id.fragmentWindow2);
        String wasComplete = createnewcardfragment.createNewCardFromInputs();

        if(wasComplete.equals("yes")) {
            bundle.putSerializable("keyToChosenAccount" , accountThatWasBeforeGoingToCreateNewCard);
            showFragmentWithBundle(bundle, new EditAccountFragment());
        }
    }

    //When Show Card button is pressed in EditAccountFragment and a card is chosen method opens
    //new EditCardFragment with this chosen card
    public void showCard (View v) {
        Bundle bundle = new Bundle();
        fragmentManager = getSupportFragmentManager();
        EditAccountFragment accountfragment = (EditAccountFragment) fragmentManager.findFragmentById(R.id.fragmentWindow2);
        CardClass card = accountfragment.getChosenCard();
        if(card != null) {
            bundle.putSerializable("keyToCard" , card);
            showFragmentWithBundle(bundle, new EditCardFragment());
        }
    }

    //When Delete button is pressed in EditAccountFragment and a card is chosen method calls fragment method
    //to delete the chosen card and "updates" the view if delete was successful
    public void deleteCard(View v) {
        Bundle bundle = new Bundle();
        fragmentManager = getSupportFragmentManager();
        EditAccountFragment accountfragment = (EditAccountFragment) fragmentManager.findFragmentById(R.id.fragmentWindow2);
        Boolean deleteWasSuccesfull = accountfragment.deleteCard();
        if(deleteWasSuccesfull == true) {
            bundle.putSerializable("keyToChosenAccount" , accountThatWasBeforeGoingToCreateNewCard);
            showFragmentWithBundle(bundle, new EditAccountFragment());
        }
    }

    //Used in getting back from EditCard and CreateNewCard
    public void getBackFromCard(View v) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("keyToChosenAccount" , accountThatWasBeforeGoingToCreateNewCard);
        showFragmentWithBundle(bundle, new EditAccountFragment());
    }

    //Calls fragment method to save updated card data and if the save was successful goes back to
    //EditAccountFramgent when Save button is pressed in EditCardFragment
    public void saveEditedCardsInfo(View v) {
        Bundle bundle = new Bundle();
        fragmentManager = getSupportFragmentManager();
        EditCardFragment editcardfragment = (EditCardFragment) fragmentManager.findFragmentById(R.id.fragmentWindow2);
        Boolean savingHappened = editcardfragment.saveEditedCard(accountThatWasBeforeGoingToCreateNewCard);
        if(savingHappened == true) {
            bundle.putSerializable("keyToChosenAccount" , accountThatWasBeforeGoingToCreateNewCard);
            showFragmentWithBundle(bundle, new EditAccountFragment());
        }
    }

    //Calls fragment method to make wanted payment and if payment was successful displays a pop up of the happened action
    //when Pay button is pressed in PaymentFragment
    public void pay(View v) {
        fragmentManager = getSupportFragmentManager();
        PaymentFragment paymentfragment = (PaymentFragment) fragmentManager.findFragmentById(R.id.fragmentWindow2);
        Boolean paymentHappened = paymentfragment.makePayment();
        if(paymentHappened == true) {
            showFragment(new PaymentFragment());
            Bundle bundle = new Bundle();
            bundle.putString("message", "Payment was made");
            DialogFragment infoWasUpdated = new popUpDialogFragment();
            infoWasUpdated.setArguments(bundle);
            infoWasUpdated.show(fragmentManager, "Dialog");
        }
    }

    //Calls fragment method to make wanted withdraw and if it's successful displays a pop up of the happened activity
    //when Withdraw button is pressed in WithdrawFragment
    public void withdraw(View v) {
        fragmentManager = getSupportFragmentManager();
        WithdrawFragment withdrawfragment = (WithdrawFragment) fragmentManager.findFragmentById(R.id.fragmentWindow2);
        Boolean withdrawHappened = withdrawfragment.makeWithdraw();
        if(withdrawHappened == true) {
            showFragment(new WithdrawFragment());
            Bundle bundle = new Bundle();
            bundle.putString("message", "Withdraw was made");
            DialogFragment infoWasUpdated = new popUpDialogFragment();
            infoWasUpdated.setArguments(bundle);
            infoWasUpdated.show(fragmentManager, "Dialog");
        }
    }

    //Calls fragment method to make wanted card payment and if it's successful displays a pop up of the happened activity
    //when Pay button is pressed in CardPaymentFragment
    public void payWithCard(View v) {
        fragmentManager = getSupportFragmentManager();
        CardPaymentFragment cardpaymentfragment = (CardPaymentFragment) fragmentManager.findFragmentById(R.id.fragmentWindow2);
        Boolean paymentHappened = cardpaymentfragment.makePayment();
        if(paymentHappened == true) {
            showFragment(new CardPaymentFragment());
            Bundle bundle = new Bundle();
            bundle.putString("message", "Payment was made with chosen card");
            DialogFragment infoWasUpdated = new popUpDialogFragment();
            infoWasUpdated.setArguments(bundle);
            infoWasUpdated.show(fragmentManager, "Dialog");
        }
    }

    //Calls fragment method to add wanted money to account and if add successful displays a pop up of the happened activity
    //when Add Money button is pressed in  AddMoneyFragment
    public void addMoney(View v) {
        fragmentManager = getSupportFragmentManager();
        AddMoneyFragment addmoneyfragment = (AddMoneyFragment) fragmentManager.findFragmentById(R.id.fragmentWindow2);
        Boolean addingHappened = addmoneyfragment.addMoney();
        if(addingHappened == true) {
            showFragment(new AddMoneyFragment());
            Bundle bundle = new Bundle();
            bundle.putString("message", "Money was added to your account");
            DialogFragment infoWasUpdated = new popUpDialogFragment();
            infoWasUpdated.setArguments(bundle);
            infoWasUpdated.show(fragmentManager, "Dialog");
        }
    }

    //Calls fragment method to save updated user data and if the save was successful displays a pop up of the happened activity
    //when Save button is pressed in UserInformationFragment
    public void saveEditedUserInfos(View v) {
        fragmentManager = getSupportFragmentManager();
        UserInformationFragment userinformationfragment = (UserInformationFragment) fragmentManager.findFragmentById(R.id.fragmentWindow2);
        Boolean updateHappened = userinformationfragment.saveUserInputs();
        if(updateHappened == true) {
            showFragment(new UserInformationFragment());
            Bundle bundle = new Bundle();
            bundle.putString("message", "Your information was updated");
            DialogFragment infoWasUpdated = new popUpDialogFragment();
            infoWasUpdated.setArguments(bundle);
            infoWasUpdated.show(fragmentManager, "Dialog");
        }
    }

    //Pop up shows info of what happened ("Payment was made" etc.) and hides automatically on click
    @Override
    public void onBasicDialogPositiveClick(DialogFragment dialog) {
    }

    public void showFragment(Fragment wantedFragment) {
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragmentWindow2, wantedFragment);
        mLayout.closeDrawer(GravityCompat.START);
        fragmentTransaction.commit();
    }

    private void showFragmentWithBundle(Bundle bundle, Fragment wantedFragment) {
        wantedFragment.setArguments(bundle);
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragmentWindow2, wantedFragment);
        mLayout.closeDrawer(GravityCompat.START);
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {
        System.out.println("Back press not allowed. Use the buttons in the app");
    }
}
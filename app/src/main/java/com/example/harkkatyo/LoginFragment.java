package com.example.harkkatyo;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

public class LoginFragment extends Fragment {
    private View view;
    private DbManager db;
    private TextView popUpText;
    private EditText givenEmail, givenPassword;
    private String strEmail, strPassword;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_login, container, false);
        db = DbManager.getInstance();

        popUpText = view.findViewById(R.id.popUpLogin);

        return view;
    }

    //Called from MainActivity to check if given login info matched with a user or admin from database
    //In case of no matches are made errormessage is displayed
    public String correctLogin() {
        db = DbManager.getInstance();
        String correct;
        givenEmail = view.findViewById(R.id.inputEmail);
        strEmail = givenEmail.getText().toString();
        givenPassword = view.findViewById(R.id.inputPassword);
        strPassword = givenPassword.getText().toString();

        if(db.loginInfoCorrectUser(strEmail, strPassword) == true) {
            //User is normal user
            correct = "user";
            //Make singleton UserClass here with just logged in user's information
            UserClass.init(strEmail);
        }else if(db.loginInfoCorrectAdmin(strEmail, strPassword) == true){
            //User is admin
            correct = "admin";
        }else{
            //Login info was wrong
            correct = "no match";
            popUpText.setVisibility(View.VISIBLE);
        }
        return correct;
    }
}

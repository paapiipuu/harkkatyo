package com.example.harkkatyo;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

public class EditCardFragment extends Fragment {

    private View view;
    private EditText paylimit, withdrawlimit;
    private TextView errorMessage;
    private Float payLimit, withdrawLimit;
    private CardClass card;
    private DbManager db;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        Bundle bundle = this.getArguments();
        card = (CardClass) bundle.getSerializable("keyToCard");
        view = inflater.inflate(R.layout.fragment_editcard, container, false);
        paylimit = view.findViewById(R.id.editText12);
        paylimit.setText(card.getpayLimit().toString(), TextView.BufferType.EDITABLE);
        withdrawlimit = view.findViewById(R.id.editText7);
        withdrawlimit.setText(card.getwithdrawLimit().toString(), TextView.BufferType.EDITABLE);
        errorMessage = view.findViewById(R.id.errormessage2);
        db = DbManager.getInstance();
        return view;
    }

    //BaseActivity calls this to take given inputs and forward them to the database to update card infos when Save button is pressed
    //Returns false if information wasn't saved (wrong inputs)
    public Boolean saveEditedCard(AccountClass cardsAccount) {
        errorMessage.setVisibility(View.INVISIBLE);
        try {
            payLimit = Float.parseFloat(paylimit.getText().toString());
            withdrawLimit = Float.parseFloat(withdrawlimit.getText().toString());
            if(payLimit < 0 || withdrawLimit < 0) {
                errorMessage.setVisibility(View.VISIBLE);
                return false;
            }else{
                db.updateCardInfo(cardsAccount, payLimit, withdrawLimit, card.getcardNumber());
                return true;
            }
        }catch(Exception e) {
            errorMessage.setVisibility(View.VISIBLE);
            return false;
        }
    }
}

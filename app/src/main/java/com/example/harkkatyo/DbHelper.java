package com.example.harkkatyo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;
public class DbHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "bank.db";
    private ContentValues bankValues = new ContentValues(), adminValues = new ContentValues();

    //Strings to create database:
    private static final String SQL_CREATE_BANK =
            "CREATE TABLE bank (bankid INTEGER PRIMARY KEY, bankname TEXT)";
    private static final String SQL_CREATE_ADMIN =
            "CREATE TABLE admin (adminid INTEGER PRIMARY KEY, bankid INTEGER, email TEXT, password TEXT, FOREIGN KEY(bankid) REFERENCES bank(bankid))";
    private static final String SQL_CREATE_USER =
            "CREATE TABLE user (email TEXT PRIMARY KEY, bankid INTEGER, firstname TEXT, lastname TEXT, password TEXT CHECK (length(password) > 0), housenumber INTEGER, street TEXT, postcode INTEGER, FOREIGN KEY(bankid) REFERENCES bank(bankid))";
    private static final String SQL_CREATE_TOWN =
            "CREATE TABLE town (bankid INTEGER, postalcode INTEGER, city TEXT, useremail TEXT, PRIMARY KEY (postalcode, useremail), FOREIGN KEY(bankid) REFERENCES bank(bankid) ON DELETE CASCADE)";
    private static final String SQL_CREATE_ACCOUNT =
            "CREATE TABLE account (accountnumber INTEGER PRIMARY KEY, balance FLOAT, useremail TEXT, payingability TEXT, FOREIGN KEY(useremail) REFERENCES user(email) ON DELETE CASCADE)";
    private static final String SQL_CREATE_CARD =
            "CREATE TABLE card (cardnumber INTEGER PRIMARY KEY, paylimit FLOAT, withdrawlimit FLOAT, accountnumber INTEGER, FOREIGN KEY(accountnumber) REFERENCES account(accountnumber) ON DELETE CASCADE)";
    private static final String SQL_CREATE_ACCOUNTACTIVITY =
            "CREATE TABLE accountactivity (activityid INTEGER PRIMARY KEY, date TEXT, accountnumber INTEGER, activity TEXT, FOREIGN KEY(accountnumber) REFERENCES account(accountnumber) ON DELETE CASCADE)";


    //initialize the database
    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    //Creates database if it doesn't already exist in the device
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("PRAGMA foreign_keys=ON;");
        db.execSQL(SQL_CREATE_BANK);
        db.execSQL(SQL_CREATE_ADMIN);
        db.execSQL(SQL_CREATE_USER);
        db.execSQL(SQL_CREATE_TOWN);
        db.execSQL(SQL_CREATE_ACCOUNT);
        db.execSQL(SQL_CREATE_CARD);
        db.execSQL(SQL_CREATE_ACCOUNTACTIVITY);

        //We make bank and admin here because they cannot or don't need to be added later
        //This is possible because we need only one bank and admin user

        bankValues.put("bankname", "Our Bank");
        db.insert("bank", null, bankValues);

        String[] take = {"bankid"};
        Cursor cursor = db.query("bank", take, null, null, null, null, null);
        cursor.moveToNext();
        adminValues.put("bankid", Integer.parseInt(cursor.getString(0)));
        cursor.close();
        adminValues.put("email", "admin@email.com");
        adminValues.put("password", "admin");
        db.insert("admin", null, adminValues);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}
}
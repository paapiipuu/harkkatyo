package com.example.harkkatyo;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;


public class EditAccountFragment extends Fragment {

    private View view;
    private TextView accountnumber, balance, errorMessage;
    private Switch mswitch;
    private UserClass user;
    private int userId;
    private DbManager db;
    private AccountClass account;
    private CardClass card = null;
    private Spinner spinner;

    //Layout consists of formatted accountnumber, balance, payingability switch, card spinner and accountActivities RecyclerView
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_editaccount, container, false);
        Bundle bundle = this.getArguments();
        account = (AccountClass) bundle.getSerializable("keyToChosenAccount");
        user = UserClass.getInstance();
        db = DbManager.getInstance();

        NumberFormat formatter = new DecimalFormat("00000000");
        DecimalFormat df = new DecimalFormat("0.00");
        accountnumber = view.findViewById(R.id.accountnumberOutput2);
        accountnumber.setText("FI" + formatter.format(account.getAccountNumber()).toString());
        balance = view.findViewById(R.id.balanceOutput);
        balance.setText(df.format(account.getBalance()).toString() + "€");

        mswitch = view.findViewById(R.id.switch2);
        mswitch.setChecked(account.getPayingAbility());
        mswitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //Switch state is updated to the database every time it's touched
                db.updateAccountInfo(isChecked, account.getAccountNumber());
            }
        });
        errorMessage = view.findViewById(R.id.textView14);

        spinner = (Spinner) view.findViewById(R.id.spinner);
        final ArrayAdapter<CardClass> adapter = new ArrayAdapter<CardClass>(getContext(), android.R.layout.simple_spinner_item, account.getCards());
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {

                //Error message is hidden when item is clicked
                errorMessage.setVisibility(View.INVISIBLE);
                card = adapter.getItem(position);

            }
            @Override
            public void onNothingSelected(AdapterView<?> adapter) {  }
        });

        RecyclerView recycler2 = view.findViewById(R.id.recyclerView2);
        recyclerViewActivities adapter2 = new recyclerViewActivities(getContext(), account.getActivities());
        recycler2.setAdapter(adapter2);
        recycler2.setLayoutManager(new LinearLayoutManager(getContext()));
        RecyclerView.ItemDecoration divider2 = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        recycler2.addItemDecoration(divider2);

        return view;
    }

    public CardClass getChosenCard() {
        if(card == null) {
            //No card was chosen
            errorMessage.setVisibility(View.VISIBLE);
        }
        CardClass cardToActivity = card;
        card = null;
        return cardToActivity;
    }

    //Is called from BaseActivity to call DbManager method to delete chosen card from database
    //If delete is successful returns true
    public Boolean deleteCard() {
        if(card == null) {
            //No card was chosen
            errorMessage.setVisibility(View.VISIBLE);
            return false;
        }
        db.deleteCard(account, card.getcardNumber());
        card = null;
        return true;
    }

}

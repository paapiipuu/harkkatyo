package com.example.harkkatyo;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class recyclerViewActivities extends RecyclerView.Adapter<recyclerViewActivities.ViewHolder> {

    private ArrayList<AccountActivityClass> activities;
    private Context mContext;

    public recyclerViewActivities(Context context, ArrayList<AccountActivityClass> activitiesArray) {
        mContext = context;
        activities = activitiesArray;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View w = LayoutInflater.from(parent.getContext()).inflate(R.layout.activititylistitem, parent, false);
        ViewHolder holder = new ViewHolder(w);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder Viewholder, int i) {
        Viewholder.dateOutput.setText((activities.get(i).getDateSTR()));
        Viewholder.activityOutput.setText(activities.get(i).getActivity());
    }

    @Override
    public int getItemCount() {
        return activities.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ConstraintLayout parent_layout;
        public TextView activityOutput, dateOutput;

        public ViewHolder(View itemView) {
            super(itemView);

            parent_layout = itemView.findViewById(R.id.activityListItemLayout);
            activityOutput = (TextView) itemView.findViewById(R.id.textView97);
            dateOutput = (TextView) itemView.findViewById(R.id.textView31);
        }
    }
}

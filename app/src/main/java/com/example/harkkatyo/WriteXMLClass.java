package com.example.harkkatyo;

import android.content.Context;
import android.util.Xml;

import org.xmlpull.v1.XmlSerializer;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;

public class WriteXMLClass {

    private static WriteXMLClass single_instance = null;
    private DbManager db;
    private ArrayList<AccountActivityClass> activities;


    private WriteXMLClass(){}

    // static method to create and get instance of Singleton class
    public static WriteXMLClass getInstance() {
        if (single_instance == null)
            single_instance = new WriteXMLClass();

        return single_instance;
    }

    //First gets all account activities from DbManager and then write a String with xmlSerializer that it returns
    private String createXMLString(){
        try {
            db = DbManager.getInstance();
            activities = db.getAllActivities();
            XmlSerializer xmlSerializer = Xml.newSerializer();
            StringWriter writer = new StringWriter();
            NumberFormat formatter = new DecimalFormat("00000000");

            xmlSerializer.setOutput(writer);

            //Start Document
            xmlSerializer.startDocument("UTF-8", true);
            xmlSerializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);

            for (AccountActivityClass activity : activities) {
                //Open Tag <activity>
                xmlSerializer.startTag("", "activity");

                xmlSerializer.startTag("", "activityid");
                xmlSerializer.attribute("", "Id", activity.getAccountnumber().toString());

                xmlSerializer.startTag("", "accountnumber");
                String accountNumber = "FI" + formatter.format(activity.getAccountnumber());
                xmlSerializer.attribute("", "Account", accountNumber);

                xmlSerializer.startTag("", "date");
                xmlSerializer.attribute("", "Date", activity.getDateSTR());

                xmlSerializer.startTag("", "description");
                xmlSerializer.attribute("", "Description", activity.getActivity());

                xmlSerializer.endTag("", "description");
                xmlSerializer.endTag("", "date");
                xmlSerializer.endTag("", "accountnumber");
                xmlSerializer.endTag("", "activityid");
                //end tag <activity>
                xmlSerializer.endTag("", "activity");
            }
            xmlSerializer.endDocument();

            return writer.toString();
        }catch(Exception e) {
            e.printStackTrace();
            return "exception";
        }
    }

    //Is called everytime normal user logs out
    //Gets created xmlString by calling createXMLString and then writes it to a file
    public void writeFile(Context context) {
        String xmlString = createXMLString();
        if (xmlString.equals("exception")) {}
        else {
            try {

                OutputStreamWriter writer = new OutputStreamWriter(context.openFileOutput("accountactivities" , Context.MODE_PRIVATE));
                writer.write(xmlString);
                writer.close();

            }catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}

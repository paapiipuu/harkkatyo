package com.example.harkkatyo;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

public class CreateNewUserFragment extends Fragment {
    private View view;
    private TextView errorMessage;
    private EditText editText, editText2, editText3, editText4, editText5, editText6, editText7, editText8;
    private DbManager db;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_createnewuser, container, false);

        errorMessage = view.findViewById(R.id.popUpRegister);
        editText = view.findViewById(R.id.editText2);
        editText2 = view.findViewById(R.id.editText);
        editText3 = view.findViewById(R.id.editText3);
        editText4 = view.findViewById(R.id.editText9);
        editText5 = view.findViewById(R.id.editText11);
        editText6 = view.findViewById(R.id.editText4);
        editText7 = view.findViewById(R.id.editText8);
        editText8 = view.findViewById(R.id.editText10);

        return view;
    }

    //Is called from MainActivity to gather given new user information and forward it to the database to create a new user
    //If some field is left empty, given email is already in use or needed values can't be parsed to numbers error message is displayed
    //In case an error happens returns false
    public String registerUserFromInputs() {
        String toActivity;
        db = DbManager.getInstance();

        if (editText.getText().toString().isEmpty() || editText2.getText().toString().isEmpty() || editText3.getText().toString().isEmpty() || editText4.getText().toString().isEmpty() || editText5.getText().toString().isEmpty() || editText6.getText().toString().isEmpty() || editText7.getText().toString().isEmpty() || editText8.getText().toString().isEmpty()) {
            errorMessage.setText("You didn't give all the required information");
            toActivity = "false";
        }else if (db.emailIsUnique(editText.getText().toString()) == false) {
            errorMessage.setText("This email is already in use");
            toActivity = "false";
        }else{
            try{
                Integer.parseInt(editText4.getText().toString());
                Integer.parseInt(editText6.getText().toString());
                if(Integer.parseInt(editText4.getText().toString()) < 0 || Integer.parseInt(editText6.getText().toString()) < 0) {
                    errorMessage.setText("Numeric values were not given right");
                    toActivity = "false";
                }
                db.createUser(editText.getText().toString(),editText2.getText().toString(),editText3.getText().toString(),
                        Integer.parseInt(editText4.getText().toString()),editText5.getText().toString(),Integer.parseInt(editText6.getText().toString()),
                        editText7.getText().toString(),editText8.getText().toString());
                toActivity = "true";
            }catch(Exception e) {
                errorMessage.setText("Numeric values were not given right");
                toActivity = "false";
            }

        }
        //returs true only if insert to database was made
        return toActivity;
    }
}

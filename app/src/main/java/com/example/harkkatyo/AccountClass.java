package com.example.harkkatyo;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;

public class AccountClass implements Serializable {

    private Integer accountNumber;
    private String email;
    private Float balance;
    private Boolean payability;
    private ArrayList<CardClass> cards;
    private ArrayList<AccountActivityClass> activities;
    private DbManager db;


    public AccountClass (Integer accountNumberFrom, Float balanceFrom, String emailFrom, Boolean payabilityFrom) {
        accountNumber = accountNumberFrom;
        balance = balanceFrom;
        payability = payabilityFrom;
        email = emailFrom;

        db = DbManager.getInstance();
        cards = db.getCards(accountNumber);
        activities = db.getActivities(accountNumber);
    }

    public Integer getAccountNumber() {
        return accountNumber;
    }
    public String getUserEmail() {return email;}
    public Float getBalance() { return balance; }
    public Boolean getPayingAbility() { return payability; }
    public ArrayList getCards() { return cards; }
    public ArrayList getActivities() { return activities; }

    //Gets called when a card is added to or deleted from the database
    public void newCards(ArrayList<CardClass> list) {
        cards.clear();
        cards = list;
    }

    //Gets called when an activity is added to the database
    public void newActivities(ArrayList<AccountActivityClass> list) {
        activities.clear();
        activities = list;
    }

    //To make spinner output look right
    @Override
    public String toString() {
        NumberFormat formatter = new DecimalFormat("00000000");
        return ("FI" + formatter.format(accountNumber));
    }
}
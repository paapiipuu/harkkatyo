package com.example.harkkatyo;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.ArrayList;


public class AdminViewFragment extends Fragment {
    private View view;
    private Spinner usersSpinner, accountsSpinner, cardsSpinner;
    private DbManager db;
    private String chosenUser;
    private AccountClass chosenAccount;
    private CardClass chosenCard;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_adminview, container, false);
        db = DbManager.getInstance();
        usersSpinner = (Spinner) view.findViewById(R.id.spinner9);
        accountsSpinner = (Spinner) view.findViewById(R.id.spinner10);
        cardsSpinner = (Spinner) view.findViewById(R.id.spinner11);

        chosenUser = null;
        chosenAccount = null;
        chosenCard = null;

        //Spinner system where next spinner gets its values when a value is chosen from the spinner before it
        //For example when admin selects the first user, their accounts will be displayed in the accountsSpinner
        final ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, db.getUserEmails());
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        usersSpinner.setAdapter(adapter1);
        usersSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {

                chosenUser = adapter1.getItem(position);
                chosenAccount = null;
                chosenCard = null;
                final ArrayAdapter<AccountClass> adapter = new ArrayAdapter<AccountClass>(getContext(), android.R.layout.simple_spinner_item, db.getAccounts(chosenUser));
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                accountsSpinner.setAdapter(adapter);
                accountsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {

                        chosenAccount = adapter.getItem(position);
                        chosenCard = null;

                        final ArrayAdapter<CardClass> adapter2 = new ArrayAdapter<CardClass>(getContext(), android.R.layout.simple_spinner_item, db.getCards(chosenAccount.getAccountNumber()));
                        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        cardsSpinner.setAdapter(adapter2);
                        cardsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {

                                chosenCard = adapter2.getItem(position);

                            }
                            @Override
                            public void onNothingSelected(AdapterView<?> adapter2) {chosenCard = null;}
                        });

                    }
                    @Override
                    public void onNothingSelected(AdapterView<?> adapter) { chosenAccount = null;}
                });
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapter1) {chosenUser = null;}
        });
        return view;
    }

    public String showChosenUser() {return chosenUser;}

    //is called from AdminActivity to call DbManager method to delete chosen user from database
    //if delete is successful returns true
    public Boolean deleteChosenUser() {
        if(chosenUser == null) { return false; }
        db.adminDeleteUser(chosenUser);
        return true;
    }

    public AccountClass showChosenAccount() {return chosenAccount;}

    //is called from AdminActivity to call DbManager method to delete chosen account from database
    //if delete is successful returns true
    public Boolean deleteChosenAccount() {
        if(chosenAccount == null){ return false; }
        Integer accountNumber = chosenAccount.getAccountNumber();
        db.adminDeleteAccount(accountNumber);
        return true;
    }

    public CardClass showChosenCard() {return chosenCard;}

    //is called from AdminActivity to call DbManager method to delete chosen user from database
    //if delete is successful returns true
    public Boolean deleteChosenCard() {
        if(chosenCard == null) { return false; }
        db.adminDeleteCard(chosenCard.getcardNumber());
        return true;
    }
}


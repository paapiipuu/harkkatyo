package com.example.harkkatyo;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.ArrayList;


public class AdminShowUserFragment extends Fragment {

    private View view;
    private String email;
    private DbManager db;
    private ArrayList<String> userInfosFromDatabase;
    private TextView textEmail, textFirstName, textLastName,  textHouseNo, textStreet, textPostCode, textCity, textPassword;


    //Gets user's information with email from the database and displays it
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_showuser, container, false);
        Bundle bundle = this.getArguments();
        email = (String) bundle.getString("email");
        db = DbManager.getInstance();
        userInfosFromDatabase = db.getUserInfos(email);

        textEmail = view.findViewById(R.id.textView69);
        textEmail.setText(email);
        textFirstName = view.findViewById(R.id.textView60);
        textFirstName.setText(userInfosFromDatabase.get(1));
        textLastName = view.findViewById(R.id.textView66);
        textLastName.setText(userInfosFromDatabase.get(2));
        textHouseNo = view.findViewById(R.id.textView72);
        textHouseNo.setText(userInfosFromDatabase.get(3));
        textStreet = view.findViewById(R.id.textView74);
        textStreet.setText(userInfosFromDatabase.get(4));
        textPostCode = view.findViewById(R.id.textView76);
        textPostCode.setText(userInfosFromDatabase.get(5));
        textCity = view.findViewById(R.id.textView78);
        textCity.setText(userInfosFromDatabase.get(6));
        textPassword = view.findViewById(R.id.textView70);
        textPassword.setText(userInfosFromDatabase.get(7));

        return view;
    }
}

package com.example.harkkatyo;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

public class CreateNewAccountFragment extends Fragment {

    private View view;
    private EditText balanceInput;
    private TextView popupText;
    private Float balance;
    private Switch simpleSwitch;
    private Boolean state;
    private DbManager db;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_createnewaccount, container, false);
        balanceInput = view.findViewById(R.id.editText13);
        simpleSwitch = (Switch) view.findViewById(R.id.switch1);
        popupText = view.findViewById(R.id.popUpNewAccount);
        return view;
    }

    //Calls DbManager method to add new account to database with inputs given by user
    //Returns true if create was successful
    public String createNewAccountFromInputs() {
        db = DbManager.getInstance();
        String toActivity = "yes";
        state = simpleSwitch.isChecked();

        try {
            balance = Float.parseFloat(balanceInput.getText().toString());
            if(balance < 0) {
                popupText.setVisibility(View.VISIBLE);
                toActivity = "no";
            }else {
                try {
                    db.createNewAccount(balance, state);
                } catch (Exception e) {
                    e.printStackTrace();
                    toActivity = "no";
                }
            }
        }catch(Exception e) {
            popupText.setVisibility(View.VISIBLE);
            toActivity = "no";
        }
        return toActivity;
    }
}

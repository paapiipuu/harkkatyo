package com.example.harkkatyo;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

public class popUpDialogFragment extends DialogFragment {
    public interface AlertDialogListener {
        public void onBasicDialogPositiveClick(DialogFragment dialog);
    }

    popUpDialogFragment.AlertDialogListener listener;

    //Makes sure DialogFragment's user is implementing AlertDialogListener
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            listener = (popUpDialogFragment.AlertDialogListener) context;
        }catch(Exception e) {
            throw new ClassCastException(getActivity().toString() + "must implement AlertDialogListener");
        }
    }

    //Layout and message for pop up
    //Gets display message from BaseActivity after some action requiring pop up is made
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(bundle.getString("message"))
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if(listener != null)
                            listener.onBasicDialogPositiveClick(popUpDialogFragment.this);
                    }
                });
        return builder.create();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }
}
